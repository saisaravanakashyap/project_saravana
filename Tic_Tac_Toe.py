board = ['-','-','-','-','-','-','-','-','-']

def display():
    print('|' + board[0] + '|' + board[1] + '|' + board[2] + '|')
    print("-------")
    print('|' + board[3] + '|' + board[4] + '|' + board[5] + '|')
    print("-------")
    print('|' + board[6] + '|' + board[7] + '|' + board[8] + '|')

def check(board):
    player_1 = 'X'
    player_2 = 'O'
    if board[0] == board[1] == board[2] == player_1 or board[0] == board[1] == board[2] == player_2:
      return True
    elif board[3] == board[4] == board[5] == player_1 or board[3] == board[4] == board[5] == player_2:
        return True 
    elif board[6] == board[7] == board[8] == player_1 or board[6] == board[7] == board[8] == player_2:
        return True
    elif board[0] == board[3] == board[6] == player_1 or board[0] == board[3] == board[6] == player_2:
        return True
    elif board[1] == board[4] == board[7] == player_1 or board[1] == board[4] == board[7] == player_2:
        return True
    elif board[2] == board[5] == board[8] == player_1 or board[2] == board[5] == board[8] == player_2:
        return True
    elif board[0] == board[4] == board[8] == player_1 or board[0] == board[4] == board[8] == player_2:
        return True
    elif board[2] == board[4] == board[6] == player_1 or board[2] == board[4] == board[6] == player_2:
        return True
    else:
        return False
    
def inp(board):
    X = int(input("Enter the position:"))
    if board[X-1] != '-':
            print("values already exist please enter new value:")
            return inp(board)
    else:
        return X
    
    
player1 = input('player 1 name:')
player2 = input('player 2 name:')
display()
for i in range(9):
    if i % 2 == 0:
        X = inp(board)
        board[X-1] = 'X'
        display()
        if check(board):
            print("Player 1 Wins")
            break
          
    else:
        X = inp(board)
        board[X-1] = 'O'
        display()
        if check(board):
            print("Player 2 Wins")
            break

print("Game Over")
          